# KLM Test Project

## Required software

### Node.js

Download [node.js](https://https://nodejs.org/) (latest LTS version)

## How to use

```
Run npm install in the root folder
```

## When installation is compleet
```
Run npm start in the root folder
```

## Known issues that cannot be solved because of the given time window
```
When pressing the next or previous button rapidly the smoothness is gone -> Would fix this by checking if the current slide is still in animation, if so ignore the click
When pressing the active indicator the current slider will re-slide into position -> Would fix this by checking if the clicked indicator is the active one, if so ignore the click
```