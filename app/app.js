// First we import an object with the slides and content
import slideItems from './data/slides';

// Then we import the styling as well
import './assets/styles/main.scss';

// Get element by selector:
const sliderElement = document.querySelector('#slider__wrapper');
const sliderIndicators = document.querySelector('.slider__indicators');

// Add slides from object to the selected element to generate the slideshow
slideItems.forEach((slide, i) => {
    // For every item in the given object we create an element and add the classes
    // After that we create the indicators at the bottom

    // I choose to generate it from an object so the slider can easaly be content manageable
    const sliderItem = document.createElement('div');
    sliderItem.className = 'slider__item';
    sliderItem.innerHTML = `<div class='slider__background' style='background-image: url(${slide.image})'><div class='slider__content'>Content for: ${slide.tekst}</div></div>`;

    // If current iteration is 0 then add last class so it's on top
    if (i === 0) sliderItem.classList.add('slider__item--last');
    sliderElement.appendChild(sliderItem);
    const sliderIndicator = document.createElement('div');
    sliderIndicator.className = 'slider__indicator';
    if (i === 0) sliderIndicator.classList.add('slider__indicator--active');
    sliderIndicator.addEventListener('click', () => {
        nextSlide(sliderElement, i);
    });
    sliderIndicators.appendChild(sliderIndicator);
});


// This function is to rotate the carousel one step forward
function nextSlide(elementList, index = false) {
    // Length of the slider __items
    const length = elementList.childElementCount - 1;

    // To handle the z-index correctly I need to remove the class on the previous slide
    // which is under the current one on top
    const lastItem = elementList.getElementsByClassName('slider__item--last')[0];
    if (lastItem) lastItem.classList.toggle('slider__item--last');

    // We select the current item so we can remove the class active,
    // which it won't be anymore after this function
    let currentItem = elementList.getElementsByClassName('slider__item--active')[0];
    if (!currentItem) currentItem = elementList.getElementsByClassName('slider__item')[0];

    // We need the current slide's position to calculate the next (previous in this case) slide
    const currentPosition = Array.prototype.indexOf.call(elementList.children, currentItem);
    let nextPosition = currentPosition < length ? currentPosition + 1 : 0;

    // if (nextPosition === index - 1) return;
    // ^^ not working correctly, but not enough time to fix this

    // If we need to go to a specific slide, we overide the nextPosition
    if (index !== false && index <= length) nextPosition = index;

    // We add the class --active to start the css transition
    elementList.children[nextPosition].classList.toggle('slider__item--active');

    // Remove --active class and add --last class to the current slide
    currentItem.classList.remove('slider__item--active', 'slider__item--backwards');
    currentItem.classList.toggle('slider__item--last');

    activateIndicator(nextPosition);
}

// This function is to rotate the carousel one step backwards
function prevSlide(elementList) {
    // Length of the slider __items
    const length = elementList.childElementCount - 1;

    // To handle the z-index correctly I need to remove the class on the previous slide
    // which is under the current one on top
    const lastItem = elementList.getElementsByClassName('slider__item--last')[0];
    if (lastItem) lastItem.classList.toggle('slider__item--last');

    // We select the current item so we can remove the class active,
    // which it won't be anymore after this function
    const currentItem = elementList.getElementsByClassName('slider__item--active')[0];

    // We need the current slide's position to calculate the next (previous in this case) slide
    const currentPosition = Array.prototype.indexOf.call(elementList.children, currentItem);
    const prevPosition = currentPosition !== 0 ? currentPosition - 1 : length;

    // We add the class --active to start the css transition
    elementList.children[prevPosition].classList.add('slider__item--active', 'slider__item--backwards');

    // Remove --active class and add --last class to the current slide
    currentItem.classList.remove('slider__item--active', 'slider__item--last', 'slider__item--backwards');
    currentItem.classList.add('slider__item--last');

    activateIndicator(prevPosition);
}

const sliderWrapperElement = document.getElementsByClassName('slider__wrapper')[0];

// bind the buttons
const previousButton = document.getElementsByClassName('slider__button--prev')[0];
if (previousButton) previousButton.addEventListener('click', () => { prevSlide(sliderElement); });


const nextButton = document.getElementsByClassName('slider__button--next')[0];
if (nextButton) nextButton.addEventListener('click', () => { nextSlide(sliderElement); });

// I created an intervalObject so we can add the interval on nested scopes
// like the 2 functions activateInterval and disableInterval
const intervalObj = {};


// Function to update the indicators
function activateIndicator(index) {
    // get the list of indicators
    const indicatorList = sliderIndicators.getElementsByClassName('slider__indicator');
    for (let i = 0; i < indicatorList.length; i++) {
        indicatorList[i].className = i === index
            ? 'slider__indicator slider__indicator--active' : 'slider__indicator';
    }
}

// Functions to start and stop the timer
function activateInterval(interval) {
    interval.interval = setInterval(() => nextSlide(sliderElement), 3000);
}

function disableInterval(interval) {
    clearInterval(interval.interval);
}

// To make sure the slider stops when mouseover, we add 2 event listeners
sliderWrapperElement.addEventListener('mouseenter', () => disableInterval(intervalObj));
sliderWrapperElement.addEventListener('mouseleave', () => activateInterval(intervalObj));
