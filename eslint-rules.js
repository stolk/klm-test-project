module.exports = {
    env: {
        browser: true,
        jasmine: true,
        node: true,
    },
    extends: 'airbnb',
    globals: {
        angular: 1,
        DEVELOP_ENV: 1,
    },
    rules: {
        'class-methods-use-this': 'off',
        'import/first': 'off',
        'import/no-extraneous-dependencies': [
            'error', {
                devDependencies: true,
                optionalDependencies: true,
                peerDependencies: false,
            },
        ],
        'import/prefer-default-export': 'off',
        indent: [
            'error',
            4, {
                SwitchCase: 1,
            },
        ],
        'new-cap': [
            'error', {
                newIsCap: false,
            },
        ],
        'no-param-reassign': [
            'error', {
                props: false,
            },
        ],
        'no-plusplus': [
            'error', {
                allowForLoopAfterthoughts: true,
            },
        ],
        'no-prototype-builtins': 'off',
        'no-use-before-define': [
            'error', {
                classes: true,
                functions: false,
            },
        ],
        'sort-imports': ['error', {
            ignoreCase: true,
            ignoreMemberSort: false,
            memberSyntaxSortOrder: [
                'all',
                'single',
                'multiple',
                'none',
            ],
        }],
        'sort-keys': ['error', 'asc', {
            caseSensitive: false,
            natural: true,
        }],
        'sort-vars': ['error', {
            ignoreCase: true,
        }],
    },
};
