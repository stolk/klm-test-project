const vars = require('../util/vars');

/**
 * Dev server configuration
 * Reference: http://webpack.github.io/docs/configuration.html#devserver
 * Reference: http://webpack.github.io/docs/webpack-dev-server.html
 */

const devServer = {
    contentBase: vars.publicFolder,
    port: vars.developmentPort,
    stats: 'minimal',
};

module.exports = devServer;
