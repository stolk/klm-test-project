const path = require('path');

// Resolve can modify how node packages and modules are picked up by webpack
const resolve = {
    // When webpack encounters key, it re-routes to value
    alias: {
        // We prefer the unminified versions for development
        angular: 'angular',
    },
    // Webpack may look in these folders for components
    modules: [
        // Standard pointer to node_modules: import _ from lodash
        path.resolve(__dirname, '../../node_modules'),
    ],
};

module.exports = resolve;
