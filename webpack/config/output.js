const env = require('../util/env');
const dev = require('../environments/dev');
const prod = require('../environments/prod');

/**
 * Output
 * Reference: http://webpack.github.io/docs/configuration.html#output
 * Should be an empty object if webpack is generating a test build
 * Karma will handle setting it up for you when it's a test build
 */

let output = dev.output;
if (env.isTest()) {
    output = {};
} else if (env.isProd()) {
    output = prod.output;
}
module.exports = output;
