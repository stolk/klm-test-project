/**
 * Entry
 * Reference: http://webpack.github.io/docs/configuration.html#entry
 * Should be undefined if webpack is generating a test build
 * Karma will set this when it's a test build
 */

const env = require('../util/env');
const prod = require('../environments/prod');

const entry = env.isTest() ? undefined : prod.entry;

module.exports = entry;
