const env = require('../util/env');
const dev = require('../environments/dev');
const prod = require('../environments/prod');
const test = require('../environments/test');
/**
 * Devtool
 * Reference: http://webpack.github.io/docs/configuration.html#devtool
 * Type of sourcemap to use per build type
 */
let devtool = dev.devtool;
if (env.isTest()) {
    devtool = test.devtool;
} else if (env.isProd()) {
    devtool = prod.devtool;
}

module.exports = devtool;
