const path = require('path');
const vars = require('../../util/vars');

const devOutput = {
    chunkFilename: '[name].bundle.js',
    filename: '[name].bundle.js',
    path: path.join(__dirname, '../../../dist'), // Points to root
    publicPath: vars.developmentPath,
};

module.exports = devOutput;
