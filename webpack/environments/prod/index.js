const entry = require('./entry');
const output = require('./output');

module.exports = {
    devtool: 'source-map',
    entry,
    output,
};
