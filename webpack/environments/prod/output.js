const path = require('path');

const prodOutput = {
    chunkFilename: '[name].[hash].js',
    filename: '[name].[hash].js',
    path: path.join(__dirname, '../../../dist'), // Points to root
    publicPath: './',
};

module.exports = prodOutput;
