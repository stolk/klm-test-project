/**
 * Environment
 * Get npm lifecycle event to identify the environment
 */
const ENV = process.env.npm_lifecycle_event;

function isTest() {
    return ENV === 'test' || ENV === 'test-watch';
}

function isProd() {
    return ENV === 'build:prod';
}

module.exports = {
    isProd,
    isTest,
};
