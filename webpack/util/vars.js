const developmentPort = 3016;
const developmentPath = `http://localhost:${developmentPort}/`;
const publicFolder = './app/public';

module.exports = {
    developmentPath,
    developmentPort,
    publicFolder,
};
