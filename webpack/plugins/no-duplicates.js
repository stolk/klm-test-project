const webpack = require('webpack');

// REMOVE DUPLICATE MODULES
// Reference: http://webpack.github.io/docs/list-of-plugins.html#dedupeplugin
// Dedupe modules in the output

const plugin = new webpack.optimize.DedupePlugin();

module.exports = plugin;
