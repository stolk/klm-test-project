const ExtractTextPlugin = require('extract-text-webpack-plugin');
const env = require('../util/env');

// EXTRACT CSS FROM FILE REFERENCES
// Reference: https://github.com/webpack/extract-text-webpack-plugin
// Disabled when in test mode or not in build mode

const plugin = new ExtractTextPlugin({
    allChunks: true,
    disable: !env.isProd(),
    filename: '[name].css',
});

module.exports = plugin;
