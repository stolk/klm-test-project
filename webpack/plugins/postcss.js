const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

// PostCSS
// Reference: https://github.com/postcss/autoprefixer-core
// Add vendor prefixes to your css
// NOTE: This is now handled in the `postcss.config.js`
// webpack2 has some issues, making the config file necessary

const plugin = new webpack.LoaderOptionsPlugin({
    options: {
        postcss: {
            plugins: [autoprefixer],
        },
    },
    test: /\.scss$/i,
});

module.exports = plugin;
