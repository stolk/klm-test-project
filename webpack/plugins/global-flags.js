const webpack = require('webpack');

// DEFINE GLOBAL FLAGS
// Reference: https://webpack.github.io/docs/list-of-plugins.html#defineplugin
// Example: Translates 'DEVELOP_ENV' into false in the bundled application

function defineGlobalFlag(keyword, boolean) {
    return new webpack.DefinePlugin({ [keyword]: JSON.stringify(Boolean(boolean)) });
}

module.exports = defineGlobalFlag;
