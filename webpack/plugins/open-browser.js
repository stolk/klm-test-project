const OpenBrowserPlugin = require('open-browser-webpack-plugin');
const vars = require('../util/vars');

// OPEN BROWSER
// Reference:   https://www.npmjs.com/package/open-browser-webpack-plugin
// Opens the browser when webpack does an initial compile

const plugin = new OpenBrowserPlugin({ url: vars.developmentPath });

module.exports = plugin;
