const webpack = require('webpack');
const path = require('path');

// Sass
// Reference: https://github.com/webpack-contrib/sass-loader
// Converts sass syntax to regular css

const plugin = new webpack.LoaderOptionsPlugin({
    options: {
        context: '/',
        sassLoader: {
            includePaths: [path.resolve(__dirname, '../../app', 'scss')],
        },
    },
});

module.exports = plugin;
