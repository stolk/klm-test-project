const AutoInjectVersionPlugin = require('webpack-auto-inject-version');

// AUTO INJECT VERSION NUMBER
// Reference: https://www.npmjs.com/package/webpack-auto-inject-version
// Update the version component with the current version

const plugin = new AutoInjectVersionPlugin({
    components: {
        AutoIncreaseVersion: false,
        InjectAsComment: false,
        InjectByTag: true,
    },
    PACKAGE_JSON_PATH: './package.json',
});

module.exports = plugin;
