const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const vars = require('../util/vars');

// COPY ASSETS
// Reference: https://github.com/kevlened/copy-webpack-plugin
// Copy assets from the dev folder
const plugin = new CopyWebpackPlugin([{
    from: path.join(__dirname, `../../${vars.publicFolder}`),
}]);

module.exports = plugin;
