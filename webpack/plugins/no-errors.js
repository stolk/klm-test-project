const webpack = require('webpack');

// ALLOW NO ERRORS
// Reference: http://webpack.github.io/docs/list-of-plugins.html#noerrorsplugin
// Only emit files when there are no errors

const plugin = new webpack.NoErrorsPlugin();

module.exports = plugin;
