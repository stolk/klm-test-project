const webpack = require('webpack');

// ProvidePlugin
// Reference: https://stackoverflow.com/a/28989476
// Defines value as import when webpack encounters key

const plugin = new webpack.ProvidePlugin({
    $: 'jquery',
    _: 'lodash',
    jQuery: 'jquery',
    moment: 'moment',
    // Window bindings are used in 3rd party plugins
    'window.$': 'jquery',
    'window._': 'lodash',
    'window.jQuery': 'jquery',
    'window.jquery': 'jquery',
    'window.moment': 'moment',
});

module.exports = plugin;
