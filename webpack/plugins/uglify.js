const webpack = require('webpack');

// UGLIFY JS
// Reference: http://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
// Minify all javascript, switch loaders to minimizing mode

const plugin = new webpack.optimize.UglifyJsPlugin({
    compress: true,
    mangle: false, // modified
    minimize: true,
    sourceMap: false,
});

module.exports = plugin;
