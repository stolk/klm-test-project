const HtmlWebpackPlugin = require('html-webpack-plugin');
const vars = require('../util/vars');

// HTML ENTRY POINT
// Reference: https://github.com/ampedandwired/html-webpack-plugin
// Render index.html (but not in testmode, of course)

const plugin = new HtmlWebpackPlugin({
    inject: 'body',
    template: `${vars.publicFolder}/index.html`,
});

module.exports = plugin;
