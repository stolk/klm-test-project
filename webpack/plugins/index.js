const env = require('../util/env');

const postcss = require('./postcss');
const sass = require('./sass');
const provide = require('./provide');
const html = require('./html');
const css = require('./css');
const version = require('./version');
const noErrors = require('./no-errors');
const noDuplicates = require('./no-duplicates');
const uglify = require('./uglify');
const assets = require('./assets');
const openBrowser = require('./open-browser');
const globalFlags = require('./global-flags');

/**
 * Plugins
 * Reference: http://webpack.github.io/docs/configuration.html#plugins
 * List: http://webpack.github.io/docs/list-of-plugins.html
 */

// Base Plugins: Always active
const plugins = [
    postcss,
    sass,
    provide,
];

// Conditional Plugins: Dev & Prod
if (!env.isTest()) {
    plugins.push(
        html,
        css,
        version);
}

// Conditional Plugins: Production only
if (env.isProd()) {
    plugins.push(
        noErrors,
        noDuplicates,
        globalFlags('DEVELOP_ENV', false),
        uglify,
        assets);
}

// Conditional Plugins: Dev only
if (!env.isTest() && !env.isProd()) {
    plugins.push(
        globalFlags('DEVELOP_ENV', true),
        openBrowser);
}

module.exports = plugins;
