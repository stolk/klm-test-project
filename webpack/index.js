// Require modules
const entry = require('./config/entry');
const output = require('./config/output');
const devtool = require('./config/devtool');
const loaders = require('./module-loaders');
const plugins = require('./plugins');
const devServer = require('./config/devServer');
const resolve = require('./config/resolve');

/**
 * Config
 * Reference: http://webpack.github.io/docs/configuration.html
 * This is the object where all configuration gets set
 */

module.exports = (function makeWebpackConfig() {
    return {
        devServer,
        devtool,
        entry,
        module: loaders,
        output,
        plugins,
        resolve,
    };
}());
