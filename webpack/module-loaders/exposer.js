// This module exposes jQuery and $ outside of webpack
// Used for the automated tests that run $ commands on the page contents

module.exports = {
    test: require.resolve('jquery'),
    use: [{
        loader: 'expose-loader',
        options: 'jQuery',
    }, {
        loader: 'expose-loader',
        options: '$',
    }],
};
