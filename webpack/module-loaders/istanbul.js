// ISTANBUL LOADER
// https://github.com/deepsweet/istanbul-instrumenter-loader
// Instrument JS files with istanbul-lib-instrument for subsequent code coverage reporting
// Skips node_modules and files that end with .spec.js

module.exports = {
    enforce: 'pre',
    exclude: [
        /node_modules/,
        /\.spec\.js$/,
    ],
    loader: 'istanbul-instrumenter-loader',
    query: { esModules: true },
    test: /\.js$/,
};
