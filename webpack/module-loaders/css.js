const ExtractTextPlugin = require('extract-text-webpack-plugin');
const env = require('../util/env');
const test = require('../environments/test');

// CSS LOADER
// Reference: https://github.com/webpack/css-loader
// Allow loading css through js
//
// Reference: https://github.com/postcss/postcss-loader
// Postprocess your css with PostCSS plugins
// Reference: https://github.com/webpack/extract-text-webpack-plugin
// Extract css files in production builds
//
// Reference: https://github.com/webpack/style-loader
// Use style-loader in development.

let loader = test.cssLoader;
if (!env.isTest()) {
    loader = ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
            { loader: 'css-loader', query: { sourceMap: true } },
            { loader: 'postcss-loader' },
            { loader: 'sass-loader', options: {/* node-sass options here */} },
        ],
    });
}

module.exports = {
    loader,
    test: /\.(css|scss|sass)$/,
};
