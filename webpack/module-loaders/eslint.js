// ESLINT
// Reference: https://github.com/MoOx/eslint-loader
// Disabled when in test mode or not in build mode

module.exports = {
    enforce: 'pre',
    exclude: /node_modules/,
    loader: 'eslint-loader',
    options: {
        failOnError: true,
        failOnWarning: false,
    },
    test: /\.jsx?$/,
};
