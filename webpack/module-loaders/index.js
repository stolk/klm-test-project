const env = require('../util/env');

const js = require('./js');
const css = require('./css');
const assets = require('./assets');
const html = require('./html');
const exposer = require('./exposer');
const istanbul = require('./istanbul');
const eslint = require('./eslint');

/**
 * Loaders
 * Reference: http://webpack.github.io/docs/configuration.html#module-loaders
 * List: http://webpack.github.io/docs/list-of-loaders.html
 * This handles most of the magic responsible for converting modules
 *
 * We use 'output' object as wrapper so we don't mention 'module' directly
 */

// Base Loaders: Always active
const output = {
    module: {
        rules: [
            js,
            css,
            assets,
            html,
            exposer,
        ],
    },
};

// Conditional Loaders: Only on TEST
if (env.isTest()) {
    output.module.rules.push(istanbul);
}

// Conditional Loaders: Dev & Prod
if (!env.isTest()) {
    output.module.rules.push(eslint);
}

module.exports = output.module;
