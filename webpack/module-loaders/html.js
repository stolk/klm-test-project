// HTML LOADER
// Reference: https://github.com/webpack/raw-loader
// Allow loading html through js

module.exports = {
    loader: 'raw-loader',
    test: /\.html$/,
};
