// JS LOADER
// Reference: https://github.com/babel/babel-loader
// Transpile .js files using babel-loader
// Compiles ES6 and ES7 into ES5 code

module.exports = {
    exclude: /node_modules/,
    loader: [
    //    { loader: 'ng-annotate-loader' },
        { loader: 'babel-loader' },
    ],
    test: /\.js$/,
};
