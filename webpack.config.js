const webpack = require('./webpack');

process.on('SIGINT', () => {
    try {
        process.stdout.write('\n//// WEBPACK CLOSED ////\n');
        process.exit(0);
    } catch (err) {
        process.stdout.write('\n//// WEBPACK ERROR ON CLOSING: ////\n');
        process.stdout.write(err);
    }
});

module.exports = webpack;
